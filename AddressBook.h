#include <iostream>
#include <vector>
#include "Person.h"
using namespace std;

class AddressBook
{
private:
	vector<Person*> contacts;

	bool addContact(Person * p);
	bool removeContact(Person * p);
public:
	AddressBook();

	unsigned int size();

	bool addContact(string name, unsigned int age);

	Person* findContact(string target);
	Person* getFirstContact();
	Person* getLastContact();

	vector<Person*> getPeopleEqualToAge(int targetAge);
	vector<Person*> getPeopleWithinAge(int min, int max);
};
