#include <iostream>
#include "AddressBook.h"
using namespace std;

int main(int argc, char ** argv)
{
	AddressBook addressBook = AddressBook();
	addressBook.addContact("Steven Zilberberg", 26);
	addressBook.addContact("Kaitlin Komeleski", 27);
	addressBook.addContact("Brian Zilberberg", 22);

	cout << "Number of Contacts: " << addressBook.size() << endl;

	Person * p2 = addressBook.getFirstContact();
	cout << p2->toString(NameWithAge) << endl;

	vector<Person*> stuff = addressBook.getPeopleWithinAge(25,30);
	cout << stuff.size() << endl;
}
