#include <iostream>
#include <string>
using namespace std;

typedef enum OutputType
{
    Invalid = 0,
    NameOnly = 1,
    NameWithAge = 2
}OutputType;

class Person
{
private:
    string name;
    unsigned int age;
public:
    Person();
    Person(string name, unsigned int age);

    string getName();
    unsigned int getAge();

    string toString();
    string toString(OutputType type);
};
