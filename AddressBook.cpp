#include "AddressBook.h"

AddressBook::AddressBook()
{
	this->contacts = vector<Person*>();
}

unsigned int AddressBook::size()
{
	return this->contacts.size();
}

bool AddressBook::addContact(string name, unsigned int age)
{
	bool success = false;
	Person * p = new Person(name, age);
	success = this->addContact(p);
	return success;
}

bool AddressBook::addContact(Person * p)
{
	bool success = false;
	this->contacts.push_back(p);
	return success;
}

bool AddressBook::removeContact(Person * p)
{
	bool success = false;

	return success;
}

Person* AddressBook::findContact(string targetName)
{
	Person * target = NULL;

	return target;
}

Person* AddressBook::getFirstContact()
{
	Person *p = this->contacts[0];
	return p;
}

Person* AddressBook::getLastContact()
{
	int size = this->contacts.size();
	if(size <= 0)
	{
		return NULL;
	}

	return this->contacts[size - 1];
}

vector<Person*> AddressBook::getPeopleEqualToAge(int targetAge)
{
	return this->getPeopleWithinAge(targetAge, targetAge);
}

vector<Person*> AddressBook::getPeopleWithinAge(int min, int max)
{
	vector<Person*> results = vector<Person*>();
	for(int c = 0; c < this->contacts.size(); c++)
	{
		int age = this->contacts[c]->getAge();
		if(age >= min && age <= max)
		{
			results.push_back(this->contacts[c]);
		}
	}
	return results;
}
