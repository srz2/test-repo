#include "Person.h"

Person::Person()
{
    this->name = "";
    this->age = 0;
}

Person::Person(string name, unsigned int age)
{
    this->name = name;
    this->age = age;
}

string Person::getName()
{
    return this->name;
}

unsigned int Person::getAge()
{
    return this->age;
}

string Person::toString()
{
    return this->toString(NameOnly);
}

string Person::toString(OutputType type)
{
    string output = "";
    switch(type)
    {
        case NameOnly:
        {
            output = this->name;
            break;
        }
        case NameWithAge:
        {
            output = this->name + "(" + to_string(this->age) + ")";
            break;
        }
        default:
        {
            break;
        }
    }
    return output;
}
